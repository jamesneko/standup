# standup

Quick little script to help me remember what I did yesterday (or even earlier today).

# Dependencies

 * Perl, Method::Signatures, File::Slurp, DateTime and List::MoreUtils.
 * Use `cpanm --installdeps .` to get these.

# Installation

 * Put it somewhere
 * Call as `standup.pl yesterday` or `standup.pl today "fixed the thing that broke"`
 * Or set up aliases
 * Or you can set up symlinks called `yesterday` and `today` in your path that point to it, and then call it as e.g. `yesterday did the thing`.
 
# Usage

 * `today "fixed the thing"` - record something you did
 * `today` - list the things you did
 * `yesterday "oh also fixed the other thing"` - retroactively do a thing yesterday
 * `yesterday` - list what you did yesterday
 * `last 14` - return the last 14 calendar days' worth of entries

# Files

 * Creates and maintains a `~/.standup.log` file

# Warning Message

 * You'll see `Any::Moose is deprecated. Please use Moo instead at /home/james.clark/perl5/lib/perl5/Method/Signatures.pm line 1293.` . It's a warning from Method::Signatures and I can't easily suppress it.
 