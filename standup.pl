#!/usr/bin/env perl

use warnings;
use strict;
use v5.16;
use Term::ANSIColor;
use File::Basename;
use Data::Dumper;

use Method::Signatures;
use File::Slurp;
use DateTime;
use List::MoreUtils qw/first_value/;


my $COMMAND = basename($0);  # What were we called as? You can make symlinks to this script.
$COMMAND = shift @ARGV || '' if $COMMAND =~ /\.pl$/;  # Alternatively, run e.g. standup.pl today "did the thing"

# All other arguments are joined into the $DONE string - if there are any.
my $DONE;
if (@ARGV) {
  $DONE = join(' ', @ARGV);
}



func note(Str $text, Str $col = 'bold cyan')
{
  say STDERR colored($text, $col);
}


func help(Str $err?)
{
  note $err, 'bold red' if $err;
  say STDERR <<EOF;
Usage: $0 <today|yesterday> "Did the thing"
       $0 <today|yesterday>
       $0 last 14
Reads or writes to a log of things done, so you can remember next standup.
You can set up symlinks to this script named after each of the commands for convenience.
EOF
  exit 1;
}


func today()
{
  return DateTime->now( time_zone => 'local' )->truncate( to => 'day' );
}


func yesterday()
{
  # On a Monday, yesterday is actually Friday.
  my $day = DateTime->now( time_zone => 'local' );
  while ($day->subtract( days => 1 )) {
    last if $day->day_of_week <= 5;  # M..F is 1..5
  }
  return $day->truncate( to => 'day' );
}


func days_ago($howmany)
{
  my $day = DateTime->now( time_zone => 'local' );
  return $day->subtract( days => $howmany )->truncate( to => 'day' );
}


func additem($day, $done)
{
  my $entries = load();
  note datetime_as_str($day), 'bold cyan';
  note " * " . $done, 'green';
  my $target = first_value { $_->{date} eq $day->ymd } @$entries;
  if ( ! $target) {
    $target = new_day($day->ymd);
    push @$entries, $target;
  }
  
  push @{$target->{items}}, $done;

  # say Dumper($entries);
  save(entries => $entries);
}


func listitems($day)
{
  my $entries = load();
  # say Dumper($entries);
  foreach my $entry (@$entries) {
    next unless $entry->{date} eq $day->ymd;
    note datetime_as_str($entry->{datetime}) . ":", 'bold cyan';
    note " * $_", 'green' foreach (@{$entry->{items}});
  }
}


func listmanyitems($day)
{
  my $entries = load();
  foreach my $entry (@$entries) {
    next unless $entry->{date} ge $day->ymd;
    note datetime_as_str($entry->{datetime}) . ":", 'bold cyan';
    note " * $_", 'green' foreach (@{$entry->{items}});
  }
}


func load(:$filename = "$ENV{HOME}/.standup.log")
{
  my @lines = read_file($filename, { binmode => ":utf8" });
  my $currday;
  my @entries;
  foreach my $line (@lines) {
    if ($line =~ /^(\d+-\d+-\d+):$/) {
      # 2020-01-01:
      if (defined $currday) {
        # We were processing one previously, save it now.
        push @entries, $currday;
      }
      $currday = new_day($1);

    } elsif ($line =~ /^ \* (.*)$/) {
      # * Did the thing
      push @{$currday->{items}}, $1;
    
    } elsif ($line =~ /^\s*$/) {
      # Blank lines are fine.

    } else {
      die "Unparseable line in file: '$line'";
    }
  }
  if (defined $currday) {
    # Save the last one
    push @entries, $currday;
  }
  return \@entries;
}


func save(:$filename = "$ENV{HOME}/.standup.log", :$entries)
{
  my @lines;
  foreach my $entry (@$entries) {
    push @lines, "$entry->{date}:\n";
    push @lines, map { " * $_\n" } @{$entry->{items}};
  }
  write_file($filename, { binmode => ":utf8" }, @lines);
}


func new_day($date, @items)
{
  my $dt;
  if ($date =~ /^(\d+)-(\d+)-(\d+)$/) {
    $dt = DateTime->new(year => $1, month => $2, day => $3);
  }
  return { date => $date, datetime => $dt, items => [ @items ] };
}


func datetime_as_str($dt)
{
  return $dt->day_name . ", " . $dt->ymd;
}


if ($DONE && $COMMAND eq 'today') {
  additem today, $DONE;

} elsif ($COMMAND eq 'today') {
  listitems today;
  
} elsif ($DONE && $COMMAND eq 'yesterday') {
  additem yesterday, $DONE;

} elsif ($COMMAND eq 'yesterday') {
  listitems yesterday;
  
} elsif ($COMMAND eq 'last') {
  listmanyitems days_ago(shift @ARGV);
  
} else {
  help("Unknown command specified: '$COMMAND'");
}

